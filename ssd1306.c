/*
 * ssd1306.c
 *
 * Created: 2/21/2018 9:26:14 PM
 *  Author: markcrai
 */ 

/* Standard includes. */
#include <string.h>

/* Kernel includes. */
#include "FreeRTOS.h"

/* Atmel library includes. */
#include "ssd1306.h"

/* Used to latch errors found during the execution of the example. */
static uint32_t error_detected = pdFALSE;

/* A buffer large enough to hold a complete page of data. */
static uint8_t data_buffer[256];

void init_ssd1306(Twi *twi_base)
{    
    freertos_twi_if freertos_twi;
    int set_speed;
    
    /* blocking_driver_options is used */
	const freertos_peripheral_options_t driver_options = {
		NULL,											/* This peripheral does not need a receive buffer, so this parameter is just set to NULL. */
		0,												/* There is no Rx buffer, so the rx buffer size is not used. */
		configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY,	/* The priority used by the TWI interrupts. */
		TWI_I2C_MASTER,									/* Communicating with the EEPROM requires the TWI to be configured as an I2C master. */
		(USE_TX_ACCESS_SEM | USE_RX_ACCESS_MUTEX | WAIT_TX_COMPLETE | WAIT_RX_COMPLETE)	/* The blocking driver is to be used, so WAIT_TX_COMPLETE and WAIT_RX_COMPLETE are set. */
	};
    
    freertos_twi = freertos_twi_master_init(twi_base, &driver_options);
    
    /* Check the port was initialized successfully. */
    if (freertos_twi != NULL) {
        /* Configure the TWI bus parameters.  Do this after calling
	    freertos_twi_master_init(). */
	    set_speed = twi_set_speed(twi_base, TWI_CLOCK_HZ, sysclk_get_cpu_hz());
	    
        if(set_speed == 0) {
            /* Create the task as described above. 
            xTaskCreate(ssd1306_init_sequence, "ssd1306 init",
            configMINIMAL_STACK_SIZE, (void *) freertos_twi, tskIDLE_PRIORITY,
            NULL);
            */
            ssd1306_init_sequence(freertos_twi);
        }        
    }
    
}

void ssd1306_send_cmd(freertos_twi_if freertos_twi)
{   
    twi_packet_t write_parameters;
    const portTickType max_block_time_ticks = 200UL / portTICK_RATE_MS;
    
    /* currently need to figure out what to use for the addr field */
    write_parameters.addr[0] = I2C_MASTER_ADDR;
    write_parameters.addr_length = 1;
    write_parameters.chip = I2C_SLAVE_ADDR;
    write_parameters.buffer = data_buffer;
    write_parameters.length = sizeof(data_buffer);
    
    if (freertos_twi_write_packet(freertos_twi, &write_parameters,
    max_block_time_ticks) != STATUS_OK) {
        error_detected = pdTRUE;
    }
}

void ssd1306_init_sequence(freertos_twi_if freertos_twi)
{    
    memset((void *) data_buffer, SSD1306_DISPLAYOFF, sizeof(SSD1306_DISPLAYOFF));    
    ssd1306_send_cmd(freertos_twi);
    /*
    ssd1306_send_cmd(freertos_twi, SSD1306_SETDISPLAYCLOCKDIV);
    ssd1306_send_cmd(freertos_twi, 0x80);
    
    ssd1306_send_cmd(freertos_twi, SSD1306_SETMULTIPLEX);
    ssd1306_send_cmd(freertos_twi, SSD1306_LCDHEIGHT - 1);
    
    ssd1306_send_cmd(freertos_twi, SSD1306_SETDISPLAYOFFSET);
    ssd1306_send_cmd(freertos_twi, 0x0);
    ssd1306_send_cmd(freertos_twi, SSD1306_SETSTARTLINE | 0x0);
    ssd1306_send_cmd(freertos_twi, SSD1306_CHARGEPUMP);
    ssd1306_send_cmd(freertos_twi, 0x14);
    ssd1306_send_cmd(freertos_twi, SSD1306_MEMORYMODE);
    ssd1306_send_cmd(freertos_twi, 0x00);
    ssd1306_send_cmd(freertos_twi, SSD1306_SEGREMAP | 0x1);
    ssd1306_send_cmd(freertos_twi, SSD1306_COMSCANDEC);
    ssd1306_send_cmd(freertos_twi, SSD1306_SETCOMPINS);                    // 0xDA
    ssd1306_send_cmd(freertos_twi, 0x12);
    ssd1306_send_cmd(freertos_twi, SSD1306_SETCONTRAST);
    ssd1306_send_cmd(freertos_twi, 0xCF);
    ssd1306_send_cmd(freertos_twi, SSD1306_SETPRECHARGE);                  // 0xd9
    ssd1306_send_cmd(freertos_twi, 0xF1);
    ssd1306_send_cmd(freertos_twi, SSD1306_SETVCOMDETECT);                 // 0xDB
    ssd1306_send_cmd(freertos_twi, 0x40);
    ssd1306_send_cmd(freertos_twi, SSD1306_DISPLAYALLON_RESUME);           // 0xA4
    ssd1306_send_cmd(freertos_twi, SSD1306_NORMALDISPLAY);                 // 0xA6

    ssd1306_send_cmd(freertos_twi, SSD1306_DEACTIVATE_SCROLL);

    ssd1306_send_cmd(freertos_twi, SSD1306_DISPLAYON); */
    
    /* A FreeRTOS task must not attempt to return from the function that
	implements it, but can delete itself. */
	vTaskDelete(NULL);
}
